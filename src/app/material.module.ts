import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import {
    MatInput,
    MatTextareaAutosize,
    MatButton,
} from '@angular/material';

@NgModule({
    imports: [
        MatInput,
        MatTextareaAutosize,
        MatButton,
        MatDialogModule
    ],
    exports: [
        MatInput,
        MatTextareaAutosize,
        MatButton,
        MatDialogModule
    ]
})

export class MaterialModule {}
