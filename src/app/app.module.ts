import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { NavComponent } from './nav/nav.component';
import { ClientManagerComponent } from './client-manager/client-manager.component';
import { AddClientComponent } from './add-client/add-client.component';
import { RouterModule, Routes } from '@angular/router';
// import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import 'hammerjs';

const appRoutes: Routes = [
  {
    path: 'user',
    component: UserManagerComponent
  },
  {
    path: 'client',
    component: ClientManagerComponent
  },
  {
    path: 'add-client',
    component: AddClientComponent
  }
];


@NgModule({
  declarations: [
    AppComponent,
    UserManagerComponent,
    NavBarComponent,
    NavComponent,
    ClientManagerComponent,
    AddClientComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    // MaterialModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
